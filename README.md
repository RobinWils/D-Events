<!--
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
-->

# D-Events
D-Events manages events.   
You will be able to export and import events as a csv file.  
This program was made for the **D-lang December event**.  

# Status
It works but it is just a simple demo/test program.

# Support
The first user group that I want to support are **GNU/Linux users**.  
Giving it windows or mac support might be a task for later.

# License
### GPLv3
This project is **free software** and is **licensed under the GPLv3** license.  
The project is mainly created so that I could learn the basics of D.

# Thanks!
Thank you for checking out this project.  
I hope that this project is interesting or educational for you. 