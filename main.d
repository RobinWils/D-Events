/*
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

// D-Events

// Global imports
import std.datetime;
import std.getopt;
import std.stdio;

// CSV file path
const string CSVFilePath = "events.csv";

// Flags
bool help, list;
string event;

// Main method
void main(string[] args) {
  // Get the current date
  auto currentTime = Clock.currTime();
  auto currentDateString = Date(currentTime.year,
                                currentTime.month,
                                currentTime.day).toSimpleString();
    
  // Get the flags
  getopt(args,
         "create-event", &event,
         "date", &currentDateString,
         "help", &help,
         "list", &list);

  // Check and use the flags
  if (help) {
    writeln("D-Events");
    writeln("________\n");
    writeln("--create-event        | create a event with the current date");
    writeln("--create-event --date | create a event with a given date");
    writeln("--list                | show today's events");
    writeln("--list --date         | shows events of a given date");   
  }
  else if (list) {
    listEvents(Date.fromSimpleString(currentDateString));
  }
  else if (event != "") {
    createEvent(event,
                Date.fromSimpleString(currentDateString));
  }
}

// Create new event
void createEvent(string eventName, Date date) {
  Event[] events = importCSVFile();
  events ~= Event(eventName, date);

  auto eventsFile = File(CSVFilePath, "w");
  foreach (Event event; events) {
    eventsFile.writeln(event.name,
                       ",",
                       event.date);
  }

  eventsFile.close();
}

// List event(s) of a specific date
void listEvents(Date date) {
  Event[] events = importCSVFile();
  writeln("Events - ", date, "\n"
          "____________________\n");
  
  foreach (Event event; events) {
    if (event.date == date) {
      writeln("- ", event.name);
    }
  }
}

// Import event(s)
Event[] importCSVFile() {
  import std.array;
  import std.file;
  
  Event[] events;
  int counter = 0;

  if (CSVFilePath.exists) {
    auto eventsFile = File(CSVFilePath, "r");

    string line;
    while ((line = eventsFile.readln()) !is null) {
      auto eventProperties = split(line, ",");
    
      events ~= Event(eventProperties[0],
                      Date.fromSimpleString(eventProperties[1]));
      counter++;
    }
  }
  return events;
}

// Event struct
private struct Event {
  string name;
  Date date;
}
